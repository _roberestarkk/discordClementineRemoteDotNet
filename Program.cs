﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using Pb.Remote;

namespace discordClementineRemoteDotNet
{
    class Program
    {
        public static BackgroundWorker ControlThread;
        public static BackgroundWorker ClementineThread;
        public static BlockingCollection<Message> Pipe;
        public static BackgroundWorker DiscordThread;
        public static Discord DiscordInstance;
        public static ClementineConnection ClementineInstance;
        static void Main(string[] args)
        {
            Pipe = new BlockingCollection<Message>();
            Console.CancelKeyPress += CtrlCHandling;
            ClementineThread = new BackgroundWorker();
            ClementineThread.WorkerSupportsCancellation = true;
            ClementineThread.DoWork += ClementineHandler;
            ClementineThread.RunWorkerAsync();

            DiscordThread = new BackgroundWorker();
            DiscordThread.WorkerSupportsCancellation = true;
            DiscordThread.DoWork += DiscordHandler;
            DiscordThread.RunWorkerAsync();

            ControlThread = new BackgroundWorker();
            ControlThread.DoWork += Controller;
            ControlThread.RunWorkerAsync();

            Utils.AppRunning.WaitOne();
            Utils.Log(LogLevel.Info, "Mainthread Stopping");
            ShutItDown();
            Utils.Log(LogLevel.Info, "Mainthread Stopped");
        }

        private static void Controller(object sender, DoWorkEventArgs e)
        {
            Console.In.ReadLineAsync().GetAwaiter().GetResult();
            Utils.AppRunning.Set();
        }

        private static void ShutItDown()
        {
            ClementineThread.CancelAsync();
            DiscordThread.CancelAsync();
            ClementineInstance.Finished.WaitOne();
            DiscordInstance.Finished.WaitOne();
        }
        private static void CtrlCHandling(object sender, ConsoleCancelEventArgs e)
        {
            e.Cancel = true;
            Utils.Log(LogLevel.Release, "Terminating Application");
            Utils.AppRunning.Set();
        }

        private static void DiscordHandler(object sender, DoWorkEventArgs e)
        {
            DiscordInstance = new Discord(DiscordThread, ref Pipe);
            DiscordInstance.RunAsync().GetAwaiter().GetResult();
        }

        private static void ClementineHandler(object sender, DoWorkEventArgs e)
        {
            ClementineInstance = new ClementineConnection(ClementineThread, ref Pipe);
            ClementineInstance.Start();
        }
    }
}