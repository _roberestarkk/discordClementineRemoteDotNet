using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Sockets;
using System.Threading;
using Google.Protobuf;
using Pb.Remote;

namespace discordClementineRemoteDotNet
{
    class ClementineConnection
    {
        BackgroundWorker MyThread;
        List<byte> Accumulator;
        Socket Client;
        int AuthCode;
        int BUFFER_SIZE = 65535;
        int Port;
        string CurrentTrack;
        string Host;
        BlockingCollection<Message> Pipe;
        public ManualResetEvent Finished;

        public ClementineConnection(
            BackgroundWorker thread,
            ref BlockingCollection<Message> pipe,
            string host = "127.0.0.1",
            int port = 5500,
            int authCode = 0
        )
        {
            MyThread = thread;
            Host = host;
            Port = port;
            AuthCode = authCode;
            Client = new Socket(SocketType.Stream, ProtocolType.Tcp);
            Accumulator = new List<byte>();
            CurrentTrack = string.Empty;
            Pipe = pipe;
            Finished = new ManualResetEvent(false);
        }

        public void SendMessage(Message message)
        {
            // Don't try to send messages if the socket is not connected
            if (Client.Connected)
            {
                using (var mystream = new NetworkStream(Client))
                {
                    message.WriteDelimitedTo(mystream);
                }
            }
        }

        public void PipeMessagesToDiscord()
        {
            // Don't try to read messages if the Socket is not connected
            if (Client.Connected)
            {
                var msgs = ParseMessagesFromBuffer();
                msgs.ForEach(x =>
                {
                    if (x.Type != MsgType.Unknown)
                    {
                        Utils.Log(LogLevel.Release,
                            "Message Received: " + x.ToString());
                        if (x.Type == MsgType.CurrentMetainfo ||
                            x.Type == MsgType.Play ||
                            x.Type == MsgType.Pause ||
                            x.Type == MsgType.Stop)
                        {
                            Pipe.Add(x);
                        }
                    }
                });
            }
        }

        public List<byte> ClearEmptyBuffer(List<byte> buffer)
        {
            if (buffer.FindIndex(x => x != 0) == -1)
            {
                buffer.Clear();
            }
            return buffer;
        }

        internal void Start()
        {
            try
            {
                Utils.Log(LogLevel.Release, "Starting Clementine");
                while (!MyThread.CancellationPending)
                {
                    Connect();
                    PipeMessagesToDiscord();
                }
                Utils.Log(LogLevel.Release, "Stopped Clementine");
                Finished.Set();
            }
            catch (System.Exception ex)
            {
                Utils.Log(LogLevel.Exception, "Clementine.Start: {0}", ex);
            }
        }

        public List<Message> ParseMessagesFromBuffer()
        {
            List<Message> msgs = new List<Message>();

            Utils.Log(LogLevel.Verbose, "Accumulator: {0}", Utils.FormatCollection(",", Accumulator.ToArray()));

            byte[] socketBuffer = new byte[BUFFER_SIZE];
            var br = Client.Receive(socketBuffer);

            Utils.Log(LogLevel.Verbose, "Read Buffer: {0}", Utils.FormatCollection(",", socketBuffer));

            if (br > 0)
            {
                Accumulator.AddRange(socketBuffer);
                Utils.Log(LogLevel.Verbose, "Accumulator + Buffer: {0}", Utils.FormatCollection(",", Accumulator.ToArray()));
                Utils.Log(LogLevel.Verbose, "Trimmed Accumulator: {0}", Utils.FormatCollection(",", Accumulator.ToArray()));
                try
                {
                    var buffer = new List<byte>(Accumulator);
                    int clearedCount = 0;
                    while (GetNextMessageFromBuffer(buffer, out clearedCount, ref msgs))
                    {
                        Utils.Log(LogLevel.Verbose, "Cleared {0} bytes", clearedCount);
                        Utils.Log(LogLevel.Verbose, "Handled Accumulator: {0}", Utils.FormatCollection(",", Accumulator.ToArray()));
                        if (clearedCount > 0)
                            Accumulator.RemoveRange(0, clearedCount);
                        buffer = ClearEmptyBuffer(buffer);
                        Utils.Log(LogLevel.Verbose, "Cleaned Accumulator: {0}", Utils.FormatCollection(",", Accumulator.ToArray()));
                    }
                    Accumulator = ClearEmptyBuffer(Accumulator);
                }
                catch (System.Exception ex)
                {
                    Utils.Log(LogLevel.Exception, "ClementineConnection.Read: {0}", ex);
                }
            }

            return msgs;
        }

        private bool GetNextMessageFromBuffer(List<byte> buffer, out int clearedCount, ref List<Message> messages)
        {
            bool keepLooping = true;
            clearedCount = 0;
            Utils.Log(LogLevel.Verbose, "Parse Buffer: " + Utils.FormatCollection(",", buffer.ToArray()));

            if (buffer.Count < 4)
            {
                return false;
            }

            // Length prefix is an int32 in big endian 
            var prefix = buffer.GetRange(0, 4);
            // Now it's in the right endianness
            if (BitConverter.IsLittleEndian)
                prefix.Reverse();

            var len = BitConverter.ToInt32(prefix.ToArray(), 0);

            buffer.RemoveRange(0, 4);

            Utils.Log(LogLevel.Verbose, "Buffer - Length: " + Utils.FormatCollection(",", buffer.ToArray()));
            // Do not parse a 0 length message
            if (len == 0)
            {
                clearedCount = 4;
                return true;
            }

            var sm = buffer.GetRange(0, len);

            Utils.Log(LogLevel.Verbose, "Single Message (Length: {0}): {1}", len, Utils.FormatCollection(",", sm.ToArray()));

            try
            {
                var msg = Message.Parser.ParseFrom(sm.ToArray());
                messages.Add(msg);
                Utils.Log(LogLevel.Verbose, "Message Parsed: " + msg.ToString());
            }
            catch (Exception ex)
            {
                if (!ex.Message.Contains("count is greater than"))
                {
                    throw;
                }
                return keepLooping;
            }

            buffer.RemoveRange(0, len);
            Utils.Log(LogLevel.Verbose, "Buffer - Message: " + Utils.FormatCollection(",", buffer.ToArray()));
            clearedCount = len + 4;

            return keepLooping;
        }

        public void Connect()
        {
            // Connect is called repeatedly to support re-trying connection without breaking the control override of the parent thread.
            // Break out again harmlessly here if already connected
            if (!Client.Connected)
            {
                try
                {
                    Utils.Log(LogLevel.Info, "Connecting Socket to host: " + Host + ":" + Port);
                    // Connect to the Socket
                    Client.Connect(Host, Port);
                    Utils.Log(LogLevel.Info, "Socket Connection Succeeded: " +
                        Client.Connected.ToString());
                    // Assemble the Clementine Connect Message
                    var connMessage = new Message();
                    connMessage.Type = MsgType.Connect;
                    Utils.Log(LogLevel.Info, "Connecting to Clementine");
                    connMessage.RequestConnect = new RequestConnect()
                    {
                        SendPlaylistSongs = false,
                        Downloader = false,
                        AuthCode = AuthCode
                    };
                    // Send the Clementine Connect Message
                    SendMessage(connMessage);
                    Utils.Log(LogLevel.Release, "Connected to Clementine");
                }
                catch (Exception ex)
                {
                    Utils.Log(LogLevel.Exception, "Clementine.Connect: {0}", ex);
                }
            }
        }
    }
}