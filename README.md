# README #

If you're using vscode, getting this program set up to develop in/with it is seemples!

If you just want to use it, try [here](http://sam-shannon.id.au/discord-clementine-integration/)

### Prerequisites to Dev ###

* VSCode
* C# Plugins
* DotNet Core v2.0.0

### How do I get set up? ###

* Clone the repo
* Open the folder with .vscode folder inside it using vscode
* dotnet restore
* probably nothing else?

### What do I need to know? ###

* Utils.LoggingLevel is set to 'Release' (when I remember) in the source control, but changing this value has many positive effects when running/debugging in VSCode.
* Utils.Log is a great function that does a fair bit of work for you.
* I am not 100% certain I know what I am doing with Async tasks, so anything that looks weird or wrong quite possibly is (and you should let me know!)